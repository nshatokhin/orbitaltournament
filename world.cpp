#include "world.h"

World::World(const QPointF &size, const qreal &gravityConst, const QPoint &colisionZonesCount, QObject *parent) : QObject(parent)
{
    setWorldSize(size);
    setGravityConst(gravityConst);
    setColisionZonesCount(colisionZonesCount);
}

World::~World()
{

}

QPointF World::worldSize() const
{
    return _physics.worldSize();
}

void World::setWorldSize(const QPointF &size)
{
    _physics.setWorldSize(size);
}

qreal World::gravityConst() const
{
    return _physics.gravityConst();
}

void World::setGravityConst(const qreal &constant)
{
    _physics.setGravityConst(constant);
}

QPoint World::colisionZonesCount() const
{
    return _physics.colisionZonesCount();
}

void World::setColisionZonesCount(const QPoint &colZoneCount)
{
    _physics.setColisionZonesCount(colZoneCount);
}

QList<Ship *> World::ships() const
{
    return _ships;
}

QList<GravitySource *> World::gravitySources() const
{
    return _gravitySources;
}

QList<Shot> World::shots() const
{
    return _shots;
}

void World::addShip(Ship *ship)
{
    _ships.append(ship);
}

void World::addGravitySource(GravitySource * source)
{
    _gravitySources.append(source);
}

void World::addShot(const Shot &shot)
{
    _shots.append(shot);
}

void World::integrate(const qreal &dt)
{
    _physics.integrate(_ships, _gravitySources, _shots, dt);
}
