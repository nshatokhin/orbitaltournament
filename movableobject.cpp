#include "movableobject.h"

#include <QtMath>

#include "gravitysource.h"
#include "linear.h"

MovableObject::MovableObject(const QPointF &position, const QPointF &speed, const qreal &maxAcceleration,
                             const qreal &mass, QObject *parent)
    : StaticObject(position, mass, parent)
{
    setSpeedVector(speed);
    setAccelerationVector(QPointF(0, 0));
    setDirectionVector(QPointF(1, 0));
    setGravityVector(QPointF(0, 0));
    setDirectionAngle(0);
    setMaxAcceleration(maxAcceleration);
}

MovableObject::MovableObject(const MovableObject &obj) : StaticObject(obj.pos(), obj.mass(), obj.parent())
{
    setSpeedVector(obj.speedVector());
    setAccelerationVector(obj.accelerationVector());
    setDirectionVector(obj.directionVector());
    setGravityVector(obj.gravityVector());
    setDirectionAngle(obj.directionAngle());
    setMaxAcceleration(obj.maxAcceleration());
}

MovableObject::~MovableObject()
{

}

MovableObject &MovableObject::operator=(const MovableObject &obj)
{
    if(this != &obj) {
        setParent(obj.parent());
        setPos(obj.pos());
        setMass(obj.mass());
        setSpeedVector(obj.speedVector());
        setAccelerationVector(obj.accelerationVector());
        setDirectionVector(obj.directionVector());
        setGravityVector(obj.gravityVector());
        setDirectionAngle(obj.directionAngle());
        setMaxAcceleration(obj.maxAcceleration());
    }

    return *this;
}

QPointF MovableObject::speedVector() const
{
    return _speedVector;
}

void MovableObject::setSpeedVector(const QPointF &newSpeedVector)
{
    _speedVector = newSpeedVector;
}

QPointF MovableObject::accelerationVector() const
{
    return _accelerationVector;
}

void MovableObject::setAccelerationVector(const QPointF &newAccelerationVector)
{
    _accelerationVector = newAccelerationVector;
}

QPointF MovableObject::directionVector() const
{
    return _directionVector;
}

void MovableObject::setDirectionVector(const QPointF &newDirectionVector)
{
    _directionVector = newDirectionVector;
}

QPointF MovableObject::gravityVector() const
{
    return _gravityVector;
}

void MovableObject::setGravityVector(const QPointF &newGravityVector)
{
    _gravityVector = newGravityVector;
}

void MovableObject::rotateDirectionVectorClockwise(const qreal &angle)
{
    qreal degrees = Linear::radiansToDegrees(directionAngle()) + angle;

    while(degrees >= 360) {
        degrees -= 360;
    }

    setDirectionAngle(Linear::degreesToRadians(degrees));
}

void MovableObject::rotateDirectionVectorCounterClockwise(const qreal &angle)
{
    qreal degrees = Linear::radiansToDegrees(directionAngle()) - angle;

    while(degrees < 0) {
        degrees += 360;
    }

    setDirectionAngle(Linear::degreesToRadians(degrees));
}

qreal MovableObject::directionAngle() const
{
    return _directionAngle;
}

void MovableObject::setDirectionAngle(const qreal &angle)
{
    if(angle != _directionAngle) {
        _directionAngle = angle;

        QPointF basicDirection(1, 0);

        setDirectionVector(Linear::vectorRotate(basicDirection, _directionAngle));
    }
}

qreal MovableObject::maxAcceleration() const
{
    return _maxAcceleration;
}

void MovableObject::setMaxAcceleration(const qreal &acceleration)
{
    _maxAcceleration = acceleration;
}

void MovableObject::integrate(const qreal &gravityConst, const QList<GravitySource *> &gravitySources, const qreal &dt)
{
    QPointF gravityForce(0, 0);
    qreal gravityAcceleration;

    for(int j=0;j<gravitySources.count();j++) {
        MovableObject * gravitySource = gravitySources.at(j);

        QPointF direction = Linear::vector(pos(), gravitySource->pos());

        if(Linear::vectorLength(direction) == 0) {
            continue;
        }

        gravityAcceleration = gravityConst * gravitySource->mass() / qPow(Linear::vectorLength(direction), 2);

        gravityForce = Linear::vectorSum(gravityForce,
                                         Linear::vectorMultiplyByScalar(direction,
                                                                        gravityAcceleration / Linear::vectorLength(direction)));
    }

    setGravityVector(gravityForce);
    setSpeedVector(Linear::newSpeed(speedVector(),
                                    Linear::vectorSum(accelerationVector(), gravityForce), dt));
    setPos(Linear::newPosition(pos(), speedVector(), dt));
}
