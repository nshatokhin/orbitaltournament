#-------------------------------------------------
#
# Project created by QtCreator 2015-05-04T02:04:38
#
#-------------------------------------------------

QT       += core gui

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = OrbitalTournament
TEMPLATE = app


SOURCES += main.cpp\
        mainwindow.cpp \
    movableobject.cpp \
    shot.cpp \
    weapon.cpp \
    ship.cpp \
    gravitysource.cpp \
    physics.cpp \
    world.cpp \
    ai.cpp \
    linear.cpp \
    staticobject.cpp

HEADERS  += mainwindow.h \
    movableobject.h \
    shot.h \
    weapon.h \
    ship.h \
    gravitysource.h \
    physics.h \
    world.h \
    ai.h \
    linear.h \
    staticobject.h

FORMS    += mainwindow.ui
