#include "mainwindow.h"
#include "ui_mainwindow.h"

#include <QKeyEvent>
#include <QPainter>

#include "linear.h"

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);

    _world.setWorldSize(QPointF(width(), height()));
    _world.setGravityConst(6.673);
    _world.setColisionZonesCount(QPoint(10, 10));

    mainLoopTimer = new QTimer();
    connect(mainLoopTimer, SIGNAL(timeout()), this, SLOT(drawFrame()));

    _dt = 0.1;
    mainLoopTimer->setInterval(1000/60);
    mainLoopTimer->start();

    _keyLeftPressed = false;
    _keyRightPressed = false;
    _spacePressed = false;
    _ctrlPressed = false;

    _playersShip = new Ship(QPointF(20, 20), QPointF(0, 0), 100, 10, Weapon(Shot(20, 1), 3 ));
    _aiShip = new Ship(QPointF(780, 580), QPointF(0.1, 0), 10, 10, Weapon(Shot(20, 1), 3 ));

    connect(_playersShip, SIGNAL(makeShot(Shot)),
            this, SLOT(makeShot(Shot)));
    connect(_aiShip, SIGNAL(makeShot(Shot)),
            this, SLOT(makeShot(Shot)));

    _world.addShip(_playersShip);
    _world.addShip(_aiShip);

    _world.addGravitySource(new GravitySource(QPointF(QPointF(this->width()/2,
                                                         this->height()/2)), 50, 10000));
}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::paintEvent(QPaintEvent *)
{
    QPainter painter(this);

    painter.setPen(Qt::black);

    QList<Ship *> ships = _world.ships();
    QList<GravitySource *> gravitySources = _world.gravitySources();
    QList<Shot> shots = _world.shots();

    GravitySource *gravity;
    for(int i=0;i<gravitySources.count();i++) {
        gravity = gravitySources.at(i);

        painter.drawEllipse(gravity->pos(), gravity->radius(), gravity->radius());
    }

    for(int i=0;i<shots.count();i++) {
        painter.setBrush(Qt::black);
        painter.drawEllipse(shots.at(i).pos(), 2, 2);
    }

    Ship * ship;
    for(int i=0;i<ships.count();i++) {
        ship = ships.at(i);

        painter.setPen(Qt::black);

        QPointF shipNose = ship->pos() + ship->directionVector() * 10;
        QPointF shipBackPoint = ship->pos() +
                Linear::vectorRotate(ship->directionVector() * 10,
                                      Linear::degreesToRadians(180));
        QPointF shipLeftPoint = shipBackPoint +
                Linear::vectorRotate(ship->directionVector() * 10,
                                      Linear::degreesToRadians(-90));
        QPointF shipRightPoint = shipBackPoint +
                Linear::vectorRotate(ship->directionVector() * 10,
                                      Linear::degreesToRadians(90));
        painter.drawLine(shipNose, shipLeftPoint);
        painter.drawLine(shipLeftPoint, shipRightPoint);
        painter.drawLine(shipRightPoint, shipNose);

        QPointF acceleration = ship->accelerationVector();
        if(Linear::vectorLength(acceleration) > 0) {
            QPointF flameVertexPoint = shipBackPoint +
                    Linear::vectorRotate(ship->accelerationVector() * 20,
                                          Linear::degreesToRadians(180));

            QPointF flameLeftPoint = shipBackPoint +
                    Linear::vectorRotate(ship->directionVector() * 5,
                                          Linear::degreesToRadians(-90));

            QPointF flameRightPoint = shipBackPoint +
                    Linear::vectorRotate(ship->directionVector() * 5,
                                          Linear::degreesToRadians(90));

            QPainterPath path;
            path.moveTo(flameVertexPoint);
            path.lineTo(flameLeftPoint);
            path.lineTo(flameRightPoint);
            path.lineTo(flameVertexPoint);

            painter.setPen (Qt::NoPen);
            painter.fillPath(path, Qt::black);
        }

        painter.setPen(Qt::red);
        painter.drawLine(ship->pos(), ship->pos() + (ship->directionVector() * 10));

        painter.setPen(Qt::blue);
        painter.drawLine(ship->pos(), ship->pos() + (ship->gravityVector() * 10));

        painter.setPen(Qt::darkGreen);
        painter.drawLine(ship->pos(), ship->pos() + (ship->speedVector()));
    }
}

void MainWindow::keyPressEvent(QKeyEvent *event)
{
    if(event->key() == Qt::Key_Left) {
        _keyLeftPressed = true;
    } else if(event->key() == Qt::Key_Right) {
        _keyRightPressed = true;
    } else if(event->key() == Qt::Key_Space) {
        _spacePressed = true;
    } else if(event->key() == Qt::Key_Control) {
        _ctrlPressed = true;
    }
}

void MainWindow::keyReleaseEvent(QKeyEvent *event)
{
    if(event->key() == Qt::Key_Left) {
        _keyLeftPressed = false;
    } else if(event->key() == Qt::Key_Right) {
        _keyRightPressed = false;
    } else if(event->key() == Qt::Key_Space) {
        _spacePressed = false;
    } else if(event->key() == Qt::Key_Control) {
        _ctrlPressed = false;
    }
}

void MainWindow::processCommands()
{
    if(_keyLeftPressed) {
        _playersShip->rotateDirectionVectorCounterClockwise(5);
    }

    if(_keyRightPressed) {
        _playersShip->rotateDirectionVectorClockwise(5);
    }

    if(_spacePressed) {
        _playersShip->turnEnginesOn(0.01);
    } else {
        _playersShip->turnEnginesOff();
    }

    if(_ctrlPressed) {
        _playersShip->startShooting();
    } else {
        _playersShip->stopShooting();
    }
}

void MainWindow::drawFrame()
{
    //_ai.integrate(_aiShip, _dt);

    processCommands();

    _world.integrate(_dt);

    update();
}

void MainWindow::makeShot(const Shot &shot)
{
    _world.addShot(shot);
}
