#ifndef WEAPON_H
#define WEAPON_H

#include <QObject>

#include "shot.h"

class Weapon : public QObject
{
    Q_OBJECT
public:
    explicit Weapon(const Shot &shot = Shot(), const qreal &cooldown = 0, QObject *parent = 0);
    Weapon(const Weapon &obj);
    ~Weapon();

    Weapon &operator=(const Weapon &obj);

    qreal cooldownTime() const;
    void setCooldownTime(const qreal &time);

    Shot shot() const;
    void setShot(const Shot &shot);

signals:

public slots:

protected:
    qreal _cooldownTime;

    Shot _shot;
};

#endif // WEAPON_H
