#ifndef STATICOBJECT_H
#define STATICOBJECT_H

#include <QObject>
#include <QPointF>

class StaticObject : public QObject
{
    Q_OBJECT
public:
    explicit StaticObject(const QPointF &position = QPointF(0, 0), const qreal &mass = 0, QObject *parent = 0);
    StaticObject(const StaticObject &obj);
    ~StaticObject();

    StaticObject& operator=(const StaticObject &obj);

    QPointF pos() const;
    void setPos(const QPointF &newPos);
    qreal x() const;
    qreal y() const;

    qreal mass() const;
    void setMass(const qreal &newMass);

    QPoint colisionZoneIndex() const;
    void setColisionZoneIndex(const QPoint &zoneIndex);

signals:

public slots:

protected:
    QPointF _pos;
    qreal _mass;
    QPoint _colisionZoneIndex;
};

#endif // STATICOBJECT_H
