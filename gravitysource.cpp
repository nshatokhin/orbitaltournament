#include "gravitysource.h"

GravitySource::GravitySource(const QPointF &position, const qreal &radius, const qreal &mass,
                             QObject *parent)
    : MovableObject(position, QPointF(0, 0), 0, mass, parent)
{
    setRadius(radius);
}

GravitySource::GravitySource(const GravitySource &obj)
    : MovableObject(obj)
{
    setRadius(obj.radius());
}

GravitySource::~GravitySource()
{

}

GravitySource &GravitySource::operator=(const GravitySource &obj)
{
    if(this != &obj) {
        setRadius(obj.radius());
        setParent(obj.parent());
        setPos(obj.pos());
        setMass(obj.mass());
        setSpeedVector(obj.speedVector());
        setAccelerationVector(obj.accelerationVector());
        setDirectionVector(obj.directionVector());
        setGravityVector(obj.gravityVector());
        setDirectionAngle(obj.directionAngle());
        setMaxAcceleration(obj.maxAcceleration());
    }

    return *this;
}

qreal GravitySource::radius() const
{
    return _radius;
}

void GravitySource::setRadius(const qreal &newRadius)
{
    _radius = newRadius;
}
