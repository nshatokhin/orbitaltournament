#include "linear.h"

#include <QtMath>

Linear::Linear()
{

}

Linear::~Linear()
{

}

QPointF Linear::vectorSum(const QPointF &vector1, const QPointF &vector2)
{
    return vector1 + vector2;
}

QPointF Linear::vectorSum(const QList<QPointF> &vectors)
{
    QPointF result(0, 0);

    for(int i=0;i<vectors.count();i++) {
        vectorSum(result, vectors.at(i));
    }

    return result;
}

qreal Linear::vectorLength(const QPointF &vector)
{
    return qSqrt(qPow(vector.x(), 2) + qPow(vector.y(), 2));
}

bool Linear::pointInCircle(const QPointF &point, const QPointF &circleCenter, const qreal &circleRadius)
{
    return qPow(point.x() - circleCenter.x(), 2) + qPow(point.y() - circleCenter.y(), 2) <= qPow(circleRadius, 2);
}

QPointF Linear::vector(const QPointF &vectorStart, const QPointF &vectorEnd)
{
    return vectorEnd - vectorStart;
}

QPointF Linear::vectorMultiplyByScalar(const QPointF &vector, const qreal &scalar)
{
    return QPointF(vector.x() * scalar, vector.y() * scalar);
}

QPointF Linear::rotatePoint(const QPointF &point, const qreal &angle)
{
    return QPointF(point.x() * qCos(angle) - point.y() * qSin(angle),
                   point.x() * qSin(angle) + point.y() * qCos(angle));
}

QPointF Linear::vectorRotate(const QPointF &vector, const qreal &angle)
{
    return rotatePoint(vector, angle);
}

QPointF Linear::vectorNormalize(const QPointF &vector)
{
    qreal length = vectorLength(vector);

    return QPointF(vector.x() / length, vector.y() / length);
}

QPointF Linear::newSpeed(const QPointF &speed, const QPointF &acceleration, const qreal &dt)
{
    return speed + vectorMultiplyByScalar(acceleration, dt);
}

QPointF Linear::newPosition(const QPointF &position, const QPointF &speed, const qreal &dt)
{
    return position + vectorMultiplyByScalar(speed, dt);
}

qreal Linear::degreesToRadians(const qreal &degrees)
{
    return degrees * M_PI / 180;
}

qreal Linear::radiansToDegrees(const qreal &radians)
{
    return radians * 180 / M_PI;
}
