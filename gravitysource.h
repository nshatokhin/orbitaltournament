#ifndef GRAVITYSOURCE_H
#define GRAVITYSOURCE_H

#include "movableobject.h"
class GravitySource : public MovableObject
{
    Q_OBJECT
public:
    explicit GravitySource(const QPointF &position = QPointF(0, 0), const qreal &radius = 0,
                           const qreal &mass = 0, QObject *parent = 0);
    GravitySource(const GravitySource &obj);
    ~GravitySource();

    GravitySource& operator=(const GravitySource &obj);

    qreal radius() const;
    void setRadius(const qreal &newRadius);

signals:

public slots:

protected:
    qreal _radius;
};

#endif // GRAVITYSOURCE_H
