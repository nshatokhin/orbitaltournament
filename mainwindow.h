#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QTimer>

#include "ai.h"
#include "world.h"

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();

private:
    Ui::MainWindow *ui;

    QTimer * mainLoopTimer;

    void paintEvent(QPaintEvent *);

    void keyPressEvent(QKeyEvent *);
    void keyReleaseEvent(QKeyEvent *);

    void processCommands();

    bool _keyLeftPressed, _keyRightPressed, _spacePressed, _ctrlPressed;

    qreal _dt;

    World _world;

    AI _ai;

    Ship * _playersShip;
    Ship * _aiShip;

private slots:
    void drawFrame();

    void makeShot(const Shot &shot);
};

#endif // MAINWINDOW_H
