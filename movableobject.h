#ifndef MOVABLEOBJECT_H
#define MOVABLEOBJECT_H

#include "staticobject.h"

class GravitySource;

class MovableObject : public StaticObject
{
    Q_OBJECT
public:
    explicit MovableObject(const QPointF &position = QPointF(0, 0), const QPointF &speed = QPointF(0, 0),
                           const qreal &maxAcceleration = 0, const qreal &mass = 0, QObject *parent = 0);
    MovableObject(const MovableObject &obj);
    ~MovableObject();

    MovableObject& operator=(const MovableObject &obj);

    QPointF speedVector() const;
    void setSpeedVector(const QPointF &newSpeedVector);
    QPointF accelerationVector() const;
    void setAccelerationVector(const QPointF &newAccelerationVector);
    QPointF directionVector() const;
    void setDirectionVector(const QPointF &newDirectionVector);
    QPointF gravityVector() const;
    void setGravityVector(const QPointF &newGravityVector);

    void rotateDirectionVectorClockwise(const qreal &angle);
    void rotateDirectionVectorCounterClockwise(const qreal &angle);

    qreal directionAngle() const;
    void setDirectionAngle(const qreal &angle);

    qreal maxAcceleration() const;
    void setMaxAcceleration(const qreal &acceleration);

    // use StaticGravitySource here
    void integrate(const qreal &gravityConst, const QList<GravitySource *> &gravitySources, const qreal &dt);

signals:

public slots:

protected:
    QPointF _speedVector;
    QPointF _accelerationVector;
    QPointF _directionVector;
    QPointF _gravityVector;

    qreal _directionAngle;

    qreal _maxAcceleration;
};

#endif // MOVABLEOBJECT_H
