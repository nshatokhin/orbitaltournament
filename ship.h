#ifndef SHIP_H
#define SHIP_H

#include "movableobject.h"
#include "shot.h"
#include "weapon.h"

class Ship : public MovableObject
{
    Q_OBJECT
public:
    explicit Ship(const QPointF &position = QPointF(0, 0), const QPointF &speed = QPointF(0, 0),
                  const qreal &maxAcceleration = 0, const qreal &mass = 0, const Weapon &weapon = Weapon(),
                  QObject *parent = 0);
    ~Ship();

    void turnEnginesOn(const qreal &power);
    void turnEnginesOff();

    bool isShooting() const;

    void startShooting();
    void stopShooting();

    void shoot(const qreal &dt);

    Weapon weapon() const;
    void setWeapon(const Weapon &wpn);

signals:
    void makeShot(const Shot &shot);

public slots:

protected:
    bool _isShooting;
    qreal _cooldownTime;

    Weapon _weapon;

};

#endif // SHIP_H
