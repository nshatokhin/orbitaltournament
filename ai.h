#ifndef AI_H
#define AI_H

#include <QObject>
#include <QPair>

#include "gravitysource.h"
#include "ship.h"

struct closer_than
{
    inline bool operator() (const QPair<GravitySource *, qreal>& left, const QPair<GravitySource *, qreal>& right)
    {
        return (left.second < right.second);
    }
};

class AI : public QObject
{
    Q_OBJECT
public:
    explicit AI(QObject *parent = 0);
    ~AI();

signals:

public slots:

protected:
    QList<GravitySource *> sortGravitySourcesByDistance(Ship * ship, const QList<GravitySource *> &sources) const;
};

#endif // AI_H
