#ifndef SHOT_H
#define SHOT_H

#include "movableobject.h"

class Shot : public MovableObject
{
    Q_OBJECT

public:
    Shot(const qreal &shotSpeed = 0, const qreal &shotMass = 0, const qreal &shotAcceleration = 0,
         const QPointF &pos = QPointF(0, 0), const QPointF &speedVector = QPointF(0, 0),
         QObject *parent = 0);
    ~Shot();

    qreal speed() const;
    void setSpeed(const qreal &speed);

protected:
    qreal _shotSpeed;
};

#endif // SHOT_H
