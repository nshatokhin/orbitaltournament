#include "weapon.h"

Weapon::Weapon(const Shot &shot, const qreal &cooldown, QObject *parent) : QObject(parent)
{
    setShot(shot);
    setCooldownTime(cooldown);
}

Weapon::Weapon(const Weapon &obj) : QObject(obj.parent())
{
    setShot(obj.shot());
    setCooldownTime(obj.cooldownTime());
}

Weapon::~Weapon()
{

}

Weapon &Weapon::operator=(const Weapon &obj)
{
    if(this != &obj) {
        setParent(obj.parent());
        setShot(obj.shot());
        setCooldownTime(obj.cooldownTime());
    }

    return *this;
}

qreal Weapon::cooldownTime() const
{
    return _cooldownTime;
}

void Weapon::setCooldownTime(const qreal &time)
{
    _cooldownTime = time;
}

Shot Weapon::shot() const
{
    return _shot;
}

void Weapon::setShot(const Shot &shot)
{
    _shot = shot;
}
