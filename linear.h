#ifndef LINEAR_H
#define LINEAR_H

#include <QList>
#include <QPointF>

class Linear
{
public:
    Linear();
    ~Linear();

    static bool pointInCircle(const QPointF &point, const QPointF &circleCenter, const qreal &circleRadius);

    static QPointF rotatePoint(const QPointF &point, const qreal &angle);
    static QPointF vectorRotate(const QPointF &vector, const qreal &angle);
    static qreal degreesToRadians(const qreal &degrees);
    static qreal radiansToDegrees(const qreal &radians);

    static qreal vectorLength(const QPointF &vector);
    static QPointF vectorSum(const QPointF &vector1, const QPointF &vector2);
    static QPointF vectorSum(const QList<QPointF> &vectors);
    static QPointF vector(const QPointF &vectorStart, const QPointF &vectorEnd);
    static QPointF vectorMultiplyByScalar(const QPointF &vector, const qreal &scalar);
    static QPointF vectorNormalize(const QPointF &vector);

    static QPointF newSpeed(const QPointF &speed, const QPointF &acceleration, const qreal &dt);
    static QPointF newPosition(const QPointF &position, const QPointF &speed, const qreal &dt);
};

#endif // LINEAR_H
