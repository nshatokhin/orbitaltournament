#include "physics.h"

Physics::Physics(const QPointF &size, const qreal &constant, const QPoint &colZonesCount, QObject *parent) : QObject(parent)
{
    setWorldSize(size);
    setGravityConst(constant);
    setColisionZonesCount(colZonesCount);

    initColisionMatrix();
}

Physics::~Physics()
{

}

QPointF Physics::worldSize() const
{
    return _worldSize;
}

void Physics::setWorldSize(const QPointF &size)
{
    _worldSize = size;
}

qreal Physics::gravityConst() const
{
    return _gravityConst;
}

void Physics::setGravityConst(const qreal &constant)
{
    _gravityConst = constant;
}

QPoint Physics::colisionZonesCount() const
{
    return _colisionZonesCount;
}

void Physics::setColisionZonesCount(const QPoint &colZonesCount)
{
    _colisionZonesCount = colZonesCount;
}

void Physics::integrate(QList<Ship *> &ships, const QList<GravitySource *> &gravitySources,
                        QList<Shot> &shots, const qreal &dt)
{
    for(int i=0;i<shots.count();i++) {
        shots[i].integrate(_gravityConst, gravitySources, dt);
    }

    for(int i=0;i<ships.count();i++) {
        if(ships[i]->isShooting()) {
            ships[i]->shoot(dt);
        }

        ships[i]->integrate(_gravityConst, gravitySources, dt);
    }
}

void Physics::initColisionMatrix()
{
    for(int i=0;i<_colisionZonesCount.x();i++) {
        _colisionMatrix.append(ColisionMatrixRow());

        for(int j=0;j<_colisionZonesCount.y();j++) {
            _colisionMatrix[i].append(QList<StaticObject*>());
        }
    }
}
