#include "staticobject.h"

StaticObject::StaticObject(const QPointF &position, const qreal &mass, QObject *parent) : QObject(parent)
{
    setPos(position);
    setMass(mass);
}

StaticObject::StaticObject(const StaticObject &obj) : QObject(obj.parent())
{
    setPos(obj.pos());
    setMass(obj.mass());
}

StaticObject::~StaticObject()
{

}

StaticObject &StaticObject::operator=(const StaticObject &obj)
{
    if(this != &obj) {
        setParent(obj.parent());
        setPos(obj.pos());
        setMass(obj.mass());
    }

    return *this;
}

QPointF StaticObject::pos() const
{
    return _pos;
}

void StaticObject::setPos(const QPointF &newPos)
{
    _pos = newPos;
}

qreal StaticObject::x() const
{
    return _pos.x();
}

qreal StaticObject::y() const
{
    return _pos.y();
}

qreal StaticObject::mass() const
{
    return _mass;
}

void StaticObject::setMass(const qreal &newMass)
{
    _mass = newMass;
}

QPoint StaticObject::colisionZoneIndex() const
{
    return _colisionZoneIndex;
}

void StaticObject::setColisionZoneIndex(const QPoint &zoneIndex)
{
    _colisionZoneIndex = zoneIndex;
}
