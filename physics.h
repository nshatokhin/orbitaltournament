#ifndef PHYSICS_H
#define PHYSICS_H

#include <QObject>

#include "gravitysource.h"
#include "ship.h"
#include "shot.h"
#include "staticobject.h"

#define ColisionMatrixRow QList<QList<StaticObject *> >
#define ColisionMatrix QList<ColisionMatrixRow>

class Physics : public QObject
{
    Q_OBJECT
public:
    explicit Physics(const QPointF &size = QPointF(), const qreal &constant = 0,
                     const QPoint &colZonesCount = QPoint(), QObject *parent = 0);
    ~Physics();

    QPointF worldSize() const;
    void setWorldSize(const QPointF &size);

    qreal gravityConst() const;
    void setGravityConst(const qreal &constant);

    QPoint colisionZonesCount() const;
    void setColisionZonesCount(const QPoint &colZoneCount);

    void integrate(QList<Ship *> &ships, const QList<GravitySource *> &gravitySources, QList<Shot> &shots, const qreal &dt);

signals:

public slots:

protected:
    QPointF _worldSize;
    qreal _gravityConst;
    QPoint _colisionZonesCount;

    ColisionMatrix _colisionMatrix;

    void initColisionMatrix();
};

#endif // PHYSICS_H
