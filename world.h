#ifndef WORLD_H
#define WORLD_H

#include <QObject>

#include "movableobject.h"
#include "gravitysource.h"
#include "physics.h"
#include "ship.h"
#include "shot.h"

class World : public QObject
{
    Q_OBJECT
public:
    explicit World(const QPointF &size = QPointF(), const qreal &gravityConst = 0,
                   const QPoint &colisionZonesCount = QPoint(), QObject *parent = 0);
    ~World();

    QPointF worldSize() const;
    void setWorldSize(const QPointF &size);

    qreal gravityConst() const;
    void setGravityConst(const qreal &constant);

    QPoint colisionZonesCount() const;
    void setColisionZonesCount(const QPoint &colZoneCount);

    QList<Ship *> ships() const;
    QList<GravitySource *> gravitySources() const;
    QList<Shot> shots() const;

    void addShip(Ship *ship);
    void addGravitySource(GravitySource *source);
    void addShot(const Shot &shot);

    void integrate(const qreal &dt);

signals:

public slots:

protected:
    QList<Ship *> _ships;
    QList<GravitySource *> _gravitySources;
    QList<Shot> _shots;

    Physics _physics;
};

#endif // WORLD_H
