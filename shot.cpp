#include "shot.h"

Shot::Shot(const qreal &shotSpeed, const qreal &shotMass, const qreal &shotAcceleration,
           const QPointF &pos, const QPointF &speedVector, QObject *parent)
    : MovableObject(pos, speedVector, shotAcceleration, shotMass, parent)
{
    setSpeed(shotSpeed);
}

Shot::~Shot()
{

}

qreal Shot::speed() const
{
    return _shotSpeed;
}

void Shot::setSpeed(const qreal &speed)
{
    _shotSpeed = speed;
}
