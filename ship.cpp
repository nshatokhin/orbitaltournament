#include "ship.h"

Ship::Ship(const QPointF &position, const QPointF &speed, const qreal &maxAcceleration,
           const qreal &mass, const Weapon &weapon, QObject *parent)
    : MovableObject(position, speed, maxAcceleration, mass, parent)
{
    stopShooting();
    setWeapon(weapon);
}

Ship::~Ship()
{

}

void Ship::turnEnginesOn(const qreal &power)
{
    if(power <= 0) {
        turnEnginesOff();
        return;
    }

    setAccelerationVector(directionVector() * (_maxAcceleration * power));
}

void Ship::turnEnginesOff()
{
    setAccelerationVector(QPointF(0, 0));
}

bool Ship::isShooting() const
{
    return _isShooting;
}

void Ship::startShooting()
{
    if(!_isShooting) {
        _isShooting = true;
        _cooldownTime = 0;
    }
}

void Ship::stopShooting()
{
    _isShooting = false;
    _cooldownTime = 0;
}

void Ship::shoot(const qreal &dt)
{
    if(_cooldownTime <= 0) {
        Shot shot = _weapon.shot();

        QPointF shotSpeedVector = _speedVector + (_directionVector * shot.speed());

        shot.setPos(_pos);
        shot.setSpeedVector(shotSpeedVector);
        emit makeShot(shot);

        _cooldownTime = _weapon.cooldownTime();
    } else {
        _cooldownTime -= dt;
    }
}

Weapon Ship::weapon() const
{
    return _weapon;
}

void Ship::setWeapon(const Weapon &wpn)
{
    _weapon = wpn;
}
