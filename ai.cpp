#include "ai.h"

#include <algorithm>

#include "linear.h"

AI::AI(QObject *parent) : QObject(parent)
{

}

AI::~AI()
{

}

QList<GravitySource *> AI::sortGravitySourcesByDistance(Ship *ship, const QList<GravitySource *> &sources) const
{
    QList<GravitySource *> result;
    QList<QPair<GravitySource *, qreal> > distances;

    QPair<GravitySource *, qreal> distance;
    for(int i=0;i<sources.count();i++) {
        distance.first = sources.at(i);

        QPointF distanceVector = Linear::vector(ship->pos(), sources.at(i)->pos());
        distance.second = Linear::vectorLength(distanceVector);

        distances.append(distance);
    }

    std::sort(distances.begin(), distances.end(), closer_than());

    for(int i=0;i<distances.count();i++){
        result.append(distances.at(i).first);
    }

    return result;
}

